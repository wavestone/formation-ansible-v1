# formation-ansible

## Set-up de la machine 
Création d'une machine Ubuntu 20.04 sur VirtualBox ([Ubuntu 20.04](https://ubuntu.com/download/desktop/thank-you?version=20.04.3&architecture=amd64)).
Une fois la machine créée, exécuter les commandes suivantes : 
- `$ sudo apt -y update`
- `$ sudo snap install --classic code`
- `$ sudo apt install -y git`
- `$ git clone https://gitlab.com/mehdi.bettiche/formation-ansible.git`
- `$ chmod +x install.sh`
- `$ ./install.sh`

## Machine packagée
La machine packagée contient : 
- Visual Studio
- Ansible 2.9
- Terraform 0.14
