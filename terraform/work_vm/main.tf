module "main"{
    source = "./infrastructure"
    participant_name = var.participant_name
}

output "liste_vms" {
  value = module.main
}
